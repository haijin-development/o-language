[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Cipher-Message Chopper] definition follows.

====================

It depends on ##([Object]).

It owns slots ##(
	[random generator]
	[integer length]
	[contents length]
	[stuff length]
).
It creates instance on ##([random generator][integer length][contents length][stuff length]).

====================

[!] [Cipher-Message Chopper] methods follow.

====================

chop bytes [byte array]
---
[chop composite]: [Cipher-Chop Composite] new after
	message id [33]
	integer length [your integer length]
	contents length [your contents length]
	stuff length [your stuff length].


Please, add contents [byte array] into [chop composite].

Result:
	[chop composite].


====================

add contents [byte array] into [chop composite]
---
[bytes size]: [byte array] size.
[chop count]: ([bytes size] / [your contents length]) ceil.
[add chop]: {[bytes] /
		[chop composite] add chop containing bytes [bytes]
			using random generator [your random generator].
	}.
[Add header chop]{}: {
		[contents length bytes]: [bytes size] little endian bytes.
		[header bytes]: [contents length bytes]
			left padded up to [your contents length] 
			with [0]. 
		[add chop] proceed with [header bytes].
	}.
[add each chop]: {[i] /
		[offset]: ([i] * [your contents length]) + [1].
		[j]: ([offset] + [your contents length]) - [1].
		[j or the end of stream]: [bytes size] min [j].
		[slice]: [byte array] copy from [offset] up to [j or the end of stream].
		[add chop] proceed with [slice].
	}.
[Add all chops]{}: {
	[0] to ([chop count] - [1]) do {[i] /
			[add each chop] proceed with [i].
		}.
	}.

[Add header chop].
[Add all chops].

====================

reconstruct from bytes [byte array]
---
[chop composite]: [Cipher-Chop Composite]
	from bytes [byte array]
	integer length [your integer length]
	contents length [your contents length]
	stuff length [your stuff length].

[chop composite] reorder chops.
[chop composite] validate chop redundancy.
[chop composite] remove chop redundancy.

Result:
	[chop composite].

====================