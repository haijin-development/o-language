[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Cipher-Chop Composite] definition follows.

====================

It depends on ##([Object]).

It owns slots ##(
	[message id]
	[integer length]
	[contents length]
	[stuff length]
	[chops]
).
It creates instance on ##([message id][integer length][contents length][stuff length]).
It creates instance on ##([integer length][contents length][stuff length]).

====================

[!] [Cipher-Chop Composite] methods follow.

====================

initialize after creation
---
[your chops]= [Sequence] new instance.
[your redundancy]= [3].

====================

add [chop]
---
[your chops] add [chop].


====================

add chop containing bytes [byte array] using random generator [random generator]
---
[contents]: Please, create contents bytes from [byte array].
[stuff indices]: Please, create stuff indices using [random generator].
[stuff]: Please, create stuff using [random generator].
[chop index]: ([your chops] size) + [1].
[chop]: [Cipher-Chop] new after
	message id [your message id]
	chop index [chop index]
	contents [contents]
	stuff indices [stuff indices]
	stuff [stuff].

Please, add [chop].

====================

create contents bytes from [byte array]
---
Result:
	[byte array] right padded up to [your contents length] with [0].

====================

create stuff indices using [random generator]
---
[stuffed length]: [your contents length] + [your stuff length].
[stuff indices]: [Sequence] new instance.
[are all indices defined?]{}: {
		([stuff indices] size) >= [your stuff length].
	}.
[new random index]{}: {
		[random generator] integer between [1] and [stuffed length].
	}.
[stuff all indices]{}: {
		{[are all indices defined?].}, as long as it is false do {
			[index]: [new random index].
			([stuff indices], does it include [index]?), if it is not so {
				[stuff indices] add [index].
			}.
		}.	
	}.

[stuff all indices].

Result:
	[stuff indices].
====================

create stuff using [random generator]
---
[stuff]: [Sequence] new instance.
[your stuff length] times repeat {
	[stuff] add ([random generator] next).
}.

Result:
	[stuff].


====================

to bytes with [integer length] and random generator [random generator]
---
[bytes]: [Sequence] new instance.

[append integer]: {[integer] /
	[little endian bytes]: [integer] little endian bytes.
	[padded]: [little endian bytes] left padded up to [integer length] with [0].
	[bytes] add all [padded].
}.
[append message id]: {[chop] /
	[append integer] proceed with ([chop] message id).
}.
[append chop index]: {[chop] /
	[append integer] proceed with ([chop] chop index).
}.
[append indices]: {[chop] /
	[bytes] add all ([chop] stuff indices).
}.
[append contents]: {[chop] /
	[stuffed length]: [your contents length] + [your stuff length].
	[stuff indices]: [chop] stuff indices.
	[contents stream]: [Circular Stream] on ([chop] contents).
	[stuff stream]: [Circular Stream] on ([chop] stuff).
	[choose next byte]: {[i] /
		([stuff indices], does it include [i]?), if it is so {
			[stuff stream] next.
		}, or otherwise {
			[contents stream] next.
		}.
	}.
	[1] to [stuffed length] do {[i] /
		[next byte]: [choose next byte] proceed with [i].
		[bytes] add [next byte].
	}.
}.
[append chop bytes]: {[chop] /
	[append message id] proceed with [chop].
	[append chop index] proceed with [chop].
	[append indices] proceed with [chop].
	[append contents] proceed with [chop].
}.
[all chops]: [Sequence] new instance.
[Create chops having redundancy]{}: {
	[your redundancy] times repeat {
		[all chops] add all [your chops].
	}.
}.
[Shuffle chops]{}: {
	[all chops] shuffle after [random generator].
}.
[Append all chop bytes]{}: {
	[all chops] each one do {[chop]/
		[append chop bytes] proceed with [chop].
	}.
}.

[Create chops having redundancy].
[Shuffle chops].
[Append all chop bytes].

Result:
	[bytes].


====================

reconstruct document
---
[bytes]: [Sequence] new instance.
[first chop]: [your chops] first.
[document chops]: [your chops] all but first one.
[actual length]: [Number] from little endian bytes ([first chop] contents).
[Concatenate all chops]{}: {
	[document chops] each one do {[chop] /
		[bytes] add all ([chop] contents).
	}.	
}.
[Remove extra bytes]{}: {
	[n]: ([bytes] size) - [actual length].
	[bytes] remove last [n].
}.

[Concatenate all chops].
[Remove extra bytes].

Result:
	[bytes].

====================

reorder chops
---
[your chops] sort after {[chop] / [chop] chop index.}.

Result:
	You.

====================
remove chop redundancy
---
[new chops]: [Sequence] new instance.
[Remove chops]{}: {
	[chop count]: ([your chops] size) / [your redundancy].
	[0] to ([chop count] - [1]) do {[index] /
		[k]: ([index] * [your redundancy]) + [1].
		[new chops] add ([your chops] at [k]).
	}.	
	[your chops]= [new chops].
}.

[Remove chops].


====================

validate chop redundancy
---
[validate chop count]{}: {
	Please, validate {([your chops] size), is it divisible by [your redundancy]?.}
		or otherwise signal {"Invalid number of chops.".}.
}.
[redundant chops at]: {[index] /
	[your chops] select those such that {[chop] / ([chop] chop index) = [index].}.
}.
[validate redundant chops]: {[chops] /
	[first chop]: [chops] first.
	Please, validate {([chops] size), is it divisible by [your redundancy]?.}
		or otherwise signal {"Invalid number of chops.".}.
	[chops] each one do {[each chop] / 
		Please, validate {[each chop] = [first chop].}
			or otherwise signal {"Invalid chop.".}.
	}.
}.
[validate each chop]: {[index] /
	[redundant chops]: [redundant chops at] proceed with [index].
	[validate redundant chops] proceed with [redundant chops].
}.
[validate all chops]{}: {
	[chop count]: ([your chops] size) / [your redundancy].
	[1] to [chop count] do {[index] /
		[validate each chop] proceed with [index].
	}.
}.

[validate chop count].
[validate all chops].

====================


[!] [Cipher-Chop Composite] factory methods follow.

====================


from bytes [byte array]
	integer length [integer length]
	contents length [contents length]
	stuff length [stuff length]
---
[header length]: ([integer length] + [integer length]) + [stuff length].
[stuffed length]: [contents length] + [stuff length].
[chop size]: [header length] + [stuffed length].
[Validate bytes length]{}: {
	Please, validate {
		([byte array] size), is it divisible by [chop size]?.
	}, or otherwise signal {"Invalid number of bytes".}.
}.
[bytes stream]: [Stream] on [byte array].
[chop composite]: [Cipher-Chop Composite] new after
		integer length [integer length]
		contents length [contents length]
		stuff length [stuff length].
[chop number]: ([byte array] size) / [chop size].
[next chop]{}: {
	[bytes]: [bytes stream] next [chop size].
	[Cipher-Chop] from bytes [bytes]
		integer length [integer length]
		contents length [contents length]
		stuff length [stuff length].
}.
[Reconstruct all chops]{}: {
	[chop number] times repeat {
		[chop composite] add [next chop].
	}.
}.

[Validate bytes length].
[Reconstruct all chops].

Result:
	[chop composite].

====================