[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [File Stream] definition follows.

====================

It depends on ##([Object]).

It owns slots ##(
	[file handle]
).
====================

[!] [File Stream] methods follow.

====================

initialize on [file handle]
---
[your file handle]= [file handle].

====================

truncate
---

#<[primitive call]; {
	[File System] truncate [your file handle].
}>.

====================

append text [string]
---

#<[primitive call]; {
	[File System] append text [string] on [your file handle].
}>.
====================


seconds since creation
---

#<[primitive call]; {
	[File System] seconds since creation of [your file handle].
}>.
====================

cr
---
Please, append text ([String] cr).
====================

close
---

Please, release.
[your file handle]= nothing.
====================

release
---

#<[primitive call]; {
	[File System] close file [your file handle].
}>.


====================

[!] [File Stream] factory methods follow.

====================

on [file handle]
---
Result:
	Please, configure new instance {[instance] /
		[instance] initialize on [file handle].
	}.

====================

open file at [path] for writing
---

#<[primitive call]; {
	[File System] open file at ([path] to string) for writing.
}>.
====================

open file at [path] for reading
---

#<[primitive call]; {
	[File System] open file at ([path] to string) for reading.
}>.
====================

open [path] for writing, then do [procedure] 
---
[file handle]: Please, open file at [path] for writing.
[stream]: Please, on [file handle].

Result:
	{
		[procedure] proceed with [stream].
	} ensure {
		[stream] close.
	}.
====================

open [path] for reading, then do [procedure] 
---
[file handle]: Please, open file at [path] for reading.
[stream]: Please, on [file handle].

Result:
	{
		[procedure] proceed with [stream].
	} ensure {
		[stream] close.
	}.
====================