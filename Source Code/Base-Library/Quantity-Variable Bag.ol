[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Variable Bag] definition follows.

====================

It depends on ##([Object]).
It owns slots ##(
	[variable powers]
).
It creates instance on ##(
	[variable powers]
).
It reads slots ##(
	[variable powers]
).
It caches calculation methods ##(
	[, is it empty?]
	[reciprocal]
	[canonical powers]
	[canonical powers set]
	[canonical string]
	[symbols]
	[unique symbol]
).

====================

[!] [Variable Bag] methods follow.

====================

initialize after creation
---

[your variable powers]= [Sequence] new instance.

====================

initialize after variable powers [variable powers]
---

#<[validation]; {
	[variable powers] is a [Sequence] of [Variable Power].
}>.

[your variable powers]= [variable powers] select those such that {[variable power] /
		([variable power] exponent) =/= [0].
	}.

====================

, is it empty?
---

Result:
	[your variable powers], is it empty?.


====================

, is it additive to [variable bag]?
---

#<[validation]; {
	[variable bag] is a [Variable Bag].
}>.

[canonical powers 1]: Please, canonical powers set.
[canonical powers 2]: [variable bag] canonical powers set.

Result:
	[canonical powers 1] = [canonical powers 2].

====================

= [variable bag]
---

#<[validation]; {
	[variable bag] is a [Variable Bag].
}>.

Result:
	[your variable powers] = ([variable bag] variable powers).

====================

each variable power do [procedure]
---

Result:
	[your variable powers] each one do [procedure].

====================

each symbol and exponent do [procedure]
---

Result:
	Please, each variable power do {[variable power] /
		[procedure] proceed with ([variable power] symbol)
			and with ([variable power] exponent).
	}.

====================

reciprocal
---

[variable powers]: ##().

Please, each variable power do {[variable power] /
	[variable powers] add ([variable power] reciprocal).
}.

Result:
	[Variable Bag] new after variable powers [variable powers].

====================

canonical powers
---

[canonical powers]: [your variable powers] proyect each one after {[variable power] /
	[variable power] canonical.
}.

Result:
	[canonical powers] sorted after {[variable power]/
		[variable power] display string.
	}.

====================

canonical powers set
---

[canonical powers]: Please, canonical powers.

Result:
	[Ordered Set] with all [canonical powers].

====================

canonical string
---

Result:
	(Please, canonical powers set) display string.

====================

display on [stream]
---

[first one variable]: [Variable Assignment] on [truth].

Please, each variable power do {[variable power] /
	([first one variable] value), if it is so {
		[first one variable] set [untruth].
	}, or otherwise {
		[stream] space.
	}.
	[stream] display [variable power].
}.

====================

symbols
---

Result:
	[your variable powers] proyect each one after {[variable power] /
		[variable power] symbol.
	}.

====================

unique symbol
---

[symbols]: Please, symbols.

#<[validation]; {
	[symbols] size is [1].
}>.

Result:
	[symbols] first.

====================

[!] [Variable Bag] factory methods follow.

====================

empty
---

[variable powers]: #().

Result:
	Please, new after variable powers [variable powers].

====================

with [symbol] exponent [exponent]
---

Result:
	Please, with [symbol] exponent [exponent] and with [""] exponent [0].

====================

with [symbol 1] exponent [exponent 1] and with [symbol 2] exponent [exponent 2]
---

#<[validation]; {
	[symbol 1] satisfies {
		([symbol 1], is it a [String]?) or {[symbol 1], is it classification?.}.
	}, description {["be either [String] or [Metric System Units]"].}.
	[symbol 2] satisfies {
		([symbol 2], is it a [String]?) or {[symbol 1], is it classification?.}.
	}, description {["be either [String] or [Metric System Units]"].}.
	[exponent 1] is a [Number].
	[exponent 2] is a [Number].
}>.

[variable powers]: ##().

([exponent 1] =/= [0]), if it is so {
	[variable exponent]: [Variable Power] symbol [symbol 1] exponent [exponent 1].
	[variable powers] add [variable exponent].
}.
([exponent 2] =/= [0]), if it is so {
	[variable exponent]: [Variable Power] symbol [symbol 2] exponent [exponent 2].
	[variable powers] add [variable exponent].
}.

Result:
	Please, new after variable powers [variable powers].

====================