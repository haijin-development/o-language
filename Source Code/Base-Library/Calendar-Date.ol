[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Date] definition follows.

====================

It depends on ##([Object]).
It owns static slots ##(
	[days of week]
).
It owns slots ##(
	[milliseconds since origin]
	[offset]
).
It creates instance on ##(
	[milliseconds since origin]
	[offset]
).
It reads slots ##(
	[milliseconds since origin]
	[offset]
).
It caches calculation methods ##(
	[year, month and day]
	[year]
	[month]
	[day of month]
	[day of week index]
	[day of week]
	[as date and time]
	[as date]
	[next]
).
It initializes after {
	[your days of week]= ##([Sunday][Monday][Tuesday][Wednesday][Thursday][Friday][Saturday]).
}.

====================

[!] [Date] methods follow.

====================

initialize after milliseconds since origin [milliseconds since origin] offset [offset]
---

[milliseconds a day]: [86400000].
[remainder]: [milliseconds since origin] remainder of division by [milliseconds a day].
[millisecs]: [milliseconds since origin] - [remainder].

[your milliseconds since origin]= [millisecs].
[your offset]= [offset].

====================

year
---

Result:
	(Please, year, month and day) first.

====================

month index
---

Result:
	(Please, year, month and day) second.

====================

month
---

[month index]:
	Please, month index.

Result:
	[Month] at index [month index].

====================

day of month
---

Result:
	(Please, year, month and day) third.

====================

day of week
---

[index]: Please, day of week index.

Result:
	[your days of week] at [index].

====================

day of week index
---

Result:
	(Please, year, month and day) fourth.

====================

<= [date]
---

Result:
	([you] = [date]) or {
		[you] < [date].
	}.

====================

= [date]
---

Result:
	[your milliseconds since origin] = ([date] milliseconds since origin).

====================

< [date]
---

Result:
	[your milliseconds since origin] < ([date] milliseconds since origin).

====================

year, month and day
---

Result:
	Please, basic year, month, day.

====================

basic year, month, day
---

#<[primitive call]; {
	[Calendar System] milliseconds [your milliseconds since origin] to year month day hour minute second.
}>.

====================

display on [stream]
---

[stream] display (Please, year) left padded up to [4] with ["0"].
[stream] append ["-"].
[stream] display (Please, month index) left padded up to [2] with ["0"].
[stream] append ["-"].
[stream] display (Please, day of month) left padded up to [2] with ["0"].

====================

as date and time
---

Result:
	[Date And Time]
		new after milliseconds since origin [your milliseconds since origin]
			offset [your offset].

====================

as date
---

Result:
	[You].

====================

+ [duration of time]
---

[amount]: [duration of time] amount.

Result:
	Decision path {
		When { [duration of time], is it a [Days]?.} do {
			[you] + [0] years, [0] months and [amount] days.}.
		When { [duration of time], is it a [Months]?.} do {
			[you] + [0] years, [amount] months and [0] days.}.
		When { [duration of time], is it a [Years]?.} do {
			[you] + [amount] years, [0] months and [0] days.}.
	}.

====================

+ [y] years, [m] months and [d] days

---

[date and time]: [you] as date and time.

[addition]: [date and time] + [y] years, [m] months, [d] days,
	[0] hours, [0] minutes and [0] seconds.

Result:
	[addition] date.

====================

to [last date], each date do [procedure]
---

[date variable]: [Variable Assignment] on [you].
[date]{}: {[date variable] value.}.
[is date in range?]{}: {[date] <= [last date].}.
[Iterate]{}: {
	{[is date in range?].}, as long as it is true do {
		[procedure] proceed with [date].
		[date variable] set ([date] next).
	}.
}.

[Iterate].

====================

next
---

Result:
	[you] + [0] years, [0] months and [1] days.

====================

[!] [Date] factory methods follow.

====================

the [day of month] of month index [month index], year [year] offset [offset]
---

[date and time]: [Date And Time]
	the [day of month] of month index [month index], year [year]
		at hour [0] minute [0] second [0]
		offset [offset].

Result:
	[date and time] date.

====================

the [n] th of [month], year [year] UTC
---

[month index]: [month] index.

Result:
	Please, the [n] of month index [month index], year [year] offset [0].

====================

from string [string]
---

[date and time]: [Date And Time] from string [string].

Result:
	[date and time] date.

====================
