[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Monomial] definition follows.

====================

It depends on ##([Object]).
It owns slots ##(
    [coefficient]
    [variable bag]
).
It reads slots ##(
    [coefficient]
    [variable bag]
).
It caches calculation methods ##(
    [, is it zero?]
    [, is it strict positive?]
    [terms]
    [symbols]
    [negated]
    [reciprocal]
).

====================

[!] [Monomial] methods follow.

====================

initialize after coefficient [coefficient] variable bag [variable bag]
---

#<[validation]; {
    [coefficient] is a [Number].
    [variable bag] is a [Variable Bag].
}>.

[your coefficient]= [coefficient].
[your variable bag]= [Variable Bag] new after variable powers ([variable bag] variable powers).

====================

as monomial
---

Result:
    [you].

====================

, is it zero?
---

Result:
    [your coefficient], is it zero?.

====================

, is it strict positive?
---

Result:
    [your coefficient], is it strict positive?.

====================

, is it additive to [monomial]?
---

#<[validation]; {
    [monomial] is a [Monomial].
}>.

Result:
    [your variable bag], is it additive to ([monomial] variable bag)?.

====================

terms
---

Result:
    [Sequence] with [you].

====================

symbols
---

Result:
    [your variable bag] symbols.

====================

reciprocal
---

[coefficient]: [your coefficient] reciprocal.
[variable bag]: [your variable bag] reciprocal.

Result:
    [Monomial] new after coefficient [coefficient] variable bag [variable bag].

====================

negated
---

[coefficient]: [your coefficient] negated.

Result:
    [Monomial] new after coefficient [coefficient] variable bag [your variable bag].

====================

+ [operand]
---

Result:
    [operand] added at right of [you].

====================

- [operand]
---

[negated]: [operand] negated.

Result:
    [you] + [negated].

====================

* [multiplier]
---

Result:
    [multiplier] multiplied at right of [you].

====================

/ [divisor]
---

[reciprocal]: [divisor] reciprocal.

Result:
    [you] * [reciprocal].

====================

< [monomial]
---

[substraction]: [you] - [monomial].

Result:
    ([substraction] =/= [0]) and {
        ([substraction] coefficient) < [0].
    }.

====================

= [monomial]
---

[is monomial]: [monomial], is it a [Monomial]?.
[same coefficient]: [your coefficient] = ([monomial] coefficient).
[same units]: [your variable bag] = ([monomial] variable bag).

Result:
    [is monomial] and {
        [same coefficient] and {[same units].}.
    }.

====================

<= [monomial]
---

[substraction]: [you] - [monomial].

Result:
    ([substraction] = [0]) or {
        ([substraction] coefficient) < [0].
    }.

====================

> [monomial]
---

[substraction]: [you] - [monomial].

Result:
    ([substraction] =/= [0]) and {
        ([substraction] coefficient) > [0].
    }.

====================

>= [monomial]
---

[substraction]: [you] - [monomial].

Result:
    ([substraction] = [0]) or {
        ([substraction] coefficient) >= [0].
    }.

====================

(- ### Double dispatch methods ### -)

added at right of [operand]
---

Result:
    [operand] add monomial [you].

====================

multiplied at right of [multiplier]
---

Result:
    [multiplier] multiply by monomial [you].

====================

add number [number]
---

#<[validation]; {
    [number] is a [Number].
}>.

[monomial]: [Monomial] coefficient [number] and no variable.

Result:
    ([number] = [0]), if it is so {
        [you].
    }, or otherwise {
        Please, add monomial [monomial].
    }. 

====================

add monomial [monomial]
---

#<[validation]; {
    [monomial] is a [Monomial].
}>.

[is it additive?]:
    [you], is it additive to [monomial]?.
[add non-additive monomial]{}: {
    [Polynomial] with all ##([you][monomial]).
}.
[add additive monomial]{}: {
    [Units Product] create monomial after {[units product] /
        [units product] set units in [your variable bag].
        [units product] set coefficient [your coefficient].
        [units product] += monomial [monomial].
    }.
}.

Result:
    [is it additive?],
        if it is so {
            [add additive monomial].
        }
       , or otherwise {
            [add non-additive monomial].
        }.

====================

add polynomial [polynomial]
---

#<[validation]; {
    [polynomial] is a [Polynomial].
}>.

Result:
    [polynomial] add monomial [you].

====================

multiply by number [number]
---

#<[validation]; {
    [number] is a [Number].
}>.

[coefficient]: [your coefficient] * [number].

Result:
    [Monomial] new after coefficient [coefficient] variable bag [your variable bag].

====================

multiply by monomial [monomial]
---

#<[validation]; {
    [monomial] is a [Monomial].
}>.

Result:
    [Units Product] create monomial after {[units product]/
        [coefficient]: [your coefficient] * ([monomial] coefficient).

        [units product] *= coefficient [coefficient].
        [units product] *= variable bag [your variable bag].
        [units product] *= variable bag ([monomial] variable bag).
    }.

====================

multiply by polynomial [polynomial]
---

#<[validation]; {
    [polynomial] is a [Polynomial].
}>.

Result:
    [polynomial] multiply by monomial [you].

====================

display on [stream]
---

[stream] display [your coefficient].
[stream] space.
[stream] display [your variable bag].

====================

of [symbol]
---

[monomial]: [Monomial] coefficient [1] symbol [symbol].

Result:
    [you] * [monomial].

====================

[!] [Monomial] factory methods follow.

====================

coefficient [number] symbol [symbol]
---

Result:
    Please, coefficient [number] symbol [symbol] exponent [1].

====================

coefficient [number] symbol [symbol] exponent [exponent]
---

#<[validation]; {
    [number] is a [Number].
    [symbol] is a [Object].
    [exponent] is a [Number].
}>.

[variable bag]: [Variable Bag] with [symbol] exponent [exponent].

Result:
    Please, new after coefficient [number] variable bag [variable bag].

====================

coefficient [number] and no variable
---

#<[validation]; {
    [number] is a [Number].
}>.

Result:
    Please, configure new instance {[monomial] /
        [variable bag]: [Variable Bag] empty.
        [monomial] initialize after coefficient [number] variable bag [variable bag].
    }.

====================

new after coefficient [coefficient] variable bag [variable bag]
---

#<[validation]; {
    [coefficient] is a [Number].
    [variable bag] is a [Variable Bag].
}>.

[Create monomial]{}: {
    Please, configure new instance {[monomial] /
        [monomial] initialize after coefficient [coefficient] variable bag [variable bag].
    }.
}.

Result:
    Decision path {
        When {[coefficient] = [0].} do {
            [0].
        }.
        When {[variable bag], is it empty?.} do {
            [coefficient].
        }.
        Otherwise do {
            [Create monomial].
        }.
    }.

====================