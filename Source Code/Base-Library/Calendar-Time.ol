[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Time] definition follows.

====================

It depends on ##([Object]).

It owns slots ##(
	[milliseconds since midnight]
	[offset]
).
It creates instance on ##(
	[milliseconds since midnight]
	[offset]
).
It caches calculation methods ##(
	[hour]
	[minute]
	[second]
).

====================

[!] [Time] methods follow.

====================

initialize after milliseconds since midnight [milliseconds since midnight] offset [offset]
---

[milliseconds a day]: [86400000].
[millisecs]: [milliseconds since midnight] remainder of division by [milliseconds a day].

[your milliseconds since midnight]= [millisecs].
[your offset]= [offset].

====================

hour
---

Result:
	[your milliseconds since midnight] integer division by [3600000].

====================

minute
---

[minute remainder]:
	[your milliseconds since midnight] remainder of division by [3600000].

Result:
	[minute remainder] integer division by [60000].

====================

second
---

[second remainder]:
	[your milliseconds since midnight] remainder of division by [60000].

Result:
	[second remainder] integer division by [1000].

====================

display on [stream]
---

[offset sign]: ([your offset], is it positive?), if it is so {
	["+"].
}, or otherwise {
	["-"].
}.

[stream] display (Please, hour) left padded up to [2] with ["0"].
[stream] append [":"].
[stream] display (Please, minute) left padded up to [2] with ["0"].
[stream] append [":"].
[stream] display (Please, second) left padded up to [2] with ["0"].
[stream] space.
[stream] append [offset sign].
[stream] display [your offset] left padded up to [2] with ["0"].

====================