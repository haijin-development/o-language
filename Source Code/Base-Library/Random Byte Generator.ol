[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Random Byte Generator] definition follows.

====================

It depends on ##([Object]).

It owns slots ##(
	[configuration table]
	[initial iteration count]
	[tail]
	[byte stream]
	[stepping stream]
	[source distortion stream]
).
It reads slots ##([configuration table]).
It writes slots ##([configuration table]).
====================

[!] [Random Byte Generator] methods follow.

====================


initialize after creation
---
	[your tail]= ##().
====================


next
---
[source byte]: [your byte stream] peek.
[sum]: [your tail] accumulate value [0] after {[total] [n] /
		([total] + [n]) remainder of division by [256].
	}.
[tail byte]: ([sum] + [source byte]) remainder of division by [256].
[Update tail]{}: {
		[your tail] add [tail byte].
		[your tail] remove first.	
	}.
[Move stream forward]{}: {
		[current position]: [your byte stream] position.
		[current step]: [your stepping stream] peek.
		[next position]: [current position] + [current step].
		[is position valid?]: [next position] < ([your byte stream] size).
		[rotate stepping]{}: {[your stepping stream] next.}.

		[is position valid?], if it is so {
				Please, set position in stream [next position].
			}, or otherwise {
				[rotate stepping].
				Please, set position in stream [1].
			}. 
	}.

[Update tail].
[Move stream forward].

Result:
	[tail byte].

====================
configuration table at [index]
---
Result:
	[your configuration table] at [index].


====================
seed [integer]
---
[configuration table size]: [your configuration table] size.
[mod]: [integer] remainder of division by ([your configuration table] size).
[table index]: [mod] + [1].

Please, set configuration at [table index].
Please, apply distortion to source [integer].
Please, set position in stream [integer].
Please, initialize stream [integer].


====================
set configuration at [table index]
---
[config]: [your configuration table] at [table index].
[source bytes]: ([config] at ["source bytes"]) copy.
[stepping]: ([config] at ["stepping"]) copy.
[source distortion]: ([config] at ["source distortion"]) copy.

[your initial iteration count]= [config] at ["initial iteration count"].
[your tail]= ([config] at ["tail"]) copy.
[your byte stream]= [Circular Stream] on [source bytes].
[your stepping stream]= [Circular Stream] on [stepping].
[your source distortion stream]= [Circular Stream] on [source distortion].

====================

apply distortion to source [seed]
---
[stream size]: [your byte stream] size.
[new value]{}: {
		(
			([your byte stream] peek) + ([your source distortion stream] peek)
		) + [seed].
	}.
[new byte]{}: {
		[new value] remainder of division by [256].
	}.
[distort all bytes in stream]{}: {
		[1] to [stream size] do {[i] /
			[your byte stream] position [i].
			[your source distortion stream] position [i].
			[your byte stream] replace value by [new byte].
		}.	
	}.

[distort all bytes in stream].


====================
set position in stream [integer]
---
[your byte stream] position [integer].


====================
initialize stream [seed]
---
[iteration count 1]: ([your byte stream] size) * [your initial iteration count].
[iteration count 2]: [seed] remainder of division by [700].
[total iteration count]: [iteration count 1] + [iteration count 2].

[total iteration count] times repeat {
	Please, next.
}.


====================

integer between [i] and [j]
---
[byte]: Please, next.
[m]: ([j] - [i]) + [1].

Result:
	([byte] remainder of division by [m]) + [i].


====================