[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Number] definition follows.

====================

It depends on ##([Object]).

====================

[!] [Number] methods follow.

====================

+ [operand]
---

Result:
	([operand], is it a [Number]?), if it is so {
		Please, primitive + [operand].
	}, or otherwise {
		[operand] added at right of [you].
	}.

====================

primitive + [number]
---

#<[primitive call]; {
	[Number System] number [you] + [number].
}>.

====================

- [operand]
---

Result:
	([operand], is it a [Number]?), if it is so {
		Please, primitive - [operand].
	}, or otherwise {
		[you] + ([operand] negated).
	}.

====================

primitive - [number]
---

#<[primitive call]; {
	[Number System] number [you] - [number].
}>.

====================

* [multiplier]
---

Result:
	([multiplier], is it a [Number]?), if it is so {
		Please, primitive * [multiplier].
	}, or otherwise {
		[multiplier] multiplied at right of [you].
	}.

====================

primitive * [number]
---

#<[primitive call]; {
	[Number System] number [you] * [number].
}>.

====================

raised to [number]
---

#<[primitive call]; {
	[Number System] number [you] raised to [number].
}>.

====================

negated
---

Result:
	[you] * [-1].

====================

reciprocal
---

Result:
	[1] / [you].

====================

/ [number]
---

Result:
	([number], is it a [Number]?), if it is so {
		Please, primitive / [number].
	}, or otherwise {
		[you] * ([number] reciprocal).
	}.

====================

primitive / [number]
---

#<[primitive call]; {
	[Number System] number [you] / [number].
}>.

====================

< [number]
---

#<[primitive call]; {
	[Number System] number [you] < [number].
}>.

====================

<= [number]
---

#<[primitive call]; {
	[Number System] number [you] <= [number].
}>.

====================

> [number]
---

#<[primitive call]; {
	[Number System] number [you] > [number].
}>.

====================

>= [number]
---

#<[primitive call]; {
	[Number System] number [you] >= [number].
}>.

====================

max [number]
---

Result:
	([you] >= [number]), if it is so {
		[you].
	}, or otherwise {
		[number].
	}.

====================

min [number]
---

Result:
	([you] < [number]), if it is so {
		[you].
	}, or otherwise {
		[number].
	}.

====================

remainder of division by [integer]
---

#<[primitive call]; {
	[Number System] remainder of [you] divided by [integer].
}>.

====================

to [n] do [procedure]
---

Result:
	([you] to [n]) each one do [procedure].

====================

times repeat [procedure]
---

#<[primitive call]; {
	[Procedure System] number [you] times repeat [procedure].
}>.

====================


mod [integer]
---

#<[primitive call]; {
	[Number System] number [you] mod [integer].
}>.

====================

integer division by [integer]
---

Result:
	([you] / [integer]) floor.

====================

ceil
---

#<[primitive call]; {
	[Number System] number [you] ceil.
}>.

====================

floor
---

#<[primitive call]; {
	[Number System] number [you] floor.
}>.
====================

little endian bytes
---

#<[primitive call]; {
	[Number System] number [you] little endian bytes.
}>.

====================

, is it zero?
---

Result:
	[you] = [0].

====================

, is it strict positive?
---

Result:
    [you] > [0].

====================


, is it divisible by [integer]?
---

Result:
	([you] mod [integer]) = [0].

====================

display on [stream]
---

[stream] append (Please, basic display string).

====================

basic display string
---

#<[primitive call]; {
	[Number System] display string [you].
}>.

====================

expressed in base [integer]
---

#<[primitive call]; {
	[Number System] number [you] expressed in base [integer].
}>.

====================

to [number]
---

Result:
	[Number Range] from [you] to [number].

====================

, is it positive?
---

Result:
	[you] >= [0].

====================

, is it negative?
---

Result:
	([you], is it positive?) not.

====================

multiply by number [number]
---

#<[validation]; {
	[number] is a [Number].
}>.

Result:
	Please, primitive * [number].

====================

multiply by monomial [monomial]
---

#<[validation]; {
	[monomial] is a [Monomial].
}>.

Result:
	[monomial] multiply by number [you].

====================

multiply by polynomial [polynomial]
---

#<[validation]; {
	[polynomial] is a [Polynomial].
}>.

Result:
	[polynomial] multiply by number [you].

====================

add monomial [monomial]
---

#<[validation]; {
	[monomial] is a [Monomial].
}>.

Result:
	[monomial] add number [you].

====================

add polynomial [polynomial]
---

#<[validation]; {
	[polynomial] is a [Polynomial].
}>.

Result:
	[polynomial] add number [you].

====================

(- ### Double dispatch methods ### -)

added at right of [operand]
---

Result:
	[operand] add number [you].

====================

multiplied at right of [multiplier]
---

Result:
	[multiplier] multiply by number [you].

====================

(- ### Units methods ### -)

seconds
---

Result:
	[Seconds] amount [you].

====================

minutes
---

Result:
	[Minutes] amount [you].

====================

hours
---

Result:
	[Hours] amount [you].

====================

days
---

Result:
	[Days] amount [you].

====================

months
---

Result:
	[Months] amount [you].

====================

years
---

Result:
	[Years] amount [you].

====================

of [symbol]
---

Result:
	[Monomial] coefficient [you] symbol [symbol].

====================

kg
---

Result:
	[you] of [Kilogram].

====================

gr
---

Result:
	[you] of [Gram].

====================

usd
---

Result:
	[you] of [USD].

====================

$
---

Result:
	[you] of [ARS].

====================

inspect
---

[Output] print ["["].
[Output] print (Please, basic display string).
[Output] print ["]"].
[Output] cr.

====================

[!] [Number] factory methods follow.

====================

from little endian bytes [bytes]
---

[bytes size]: [bytes] size.
[factor]: [Variable Assignment] on [1].
[n]: [Variable Assignment] on [0].

[bytes size] to [1] do {[i] /
	[byte]: [bytes] at [i].
	[f]: [factor] value.
	[n] += ([byte] * [f]).
	[factor] *= [256].
}.

Result:
	[n] value.

====================
