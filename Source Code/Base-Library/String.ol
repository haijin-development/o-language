[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [String] definition follows.

====================

It depends on ##([Object]).

====================

[!] [String] methods follow.

====================

to byte sequence
---

#<[primitive call]; {
	[String System] to byte sequence [you].
}>.

====================

size
---

#<[primitive call]; {
	[String System] size of [you].
}>.

====================

, is it empty?
---

Result:
	(Please, size) = [0].

====================

, is it not empty?
---

Result:
	([you], is it empty?) not.

====================

display on [stream]
---

[stream] append [you].

====================

-- [string]
---

#<[primitive call]; {
	[String System] append [you] with [string].
}>.

====================

< [string]
---

#<[primitive call]; {
	[String System] string [you] lower than [string].
}>.

====================

<= [string]
---

Result:
	([you] = [string]) or {
		[you] < [string].
	}.

====================


split [string]
---

#<[primitive call]; {
	[String System] split [you] string [string].
}>.

====================

join [array of string]
---

#<[primitive call]; {
	[String System] join [you] array [array of string].
}>.

====================

trim whitespace
---

#<[primitive call]; {
	[String System] trim whitespace in [you].
}>.

====================

left padded up to [length] with [string]
---

[size]: Please, size.
[n]: ([length] > [size]), if it is so {
		[length] - [size].
	}, or otherwise {
		[0].
	}.

Result:
	[String Stream] contents after {[stream] /
		[n] times repeat {
			[stream] append [string].
		}.
		[stream] append [you].
	}.

====================

replace all [substring] by [new string]
---

#<[primitive call]; {
	[String System] in [you] replace all [substring] by [new string].
}>.

====================

inspect
---

[Output] print ["["""].
[Output] print [you].
[Output] print ["""]"].
[Output] cr.

====================

[!] [String] factory methods follow.

====================

from bytes [bytes]
---

#<[primitive call]; {
	[String System] from bytes [bytes].
}>.
====================

cr
---

Result:
	Please, at character code [10].

====================

at character code [integer]
---

#<[primitive call]; {
	[String System] at character code [integer].
}>.


====================
