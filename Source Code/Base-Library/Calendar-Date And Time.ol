[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Date And Time] definition follows.

====================

It depends on ##([Object]).

It owns slots ##(
	[milliseconds since origin]
	[offset]
).
It creates instance on ##(
	[milliseconds since origin]
	[offset]
).
It reads slots ##(
	[milliseconds since origin]
	[offset]
).
It caches calculation methods ##(
	[date]
	[time]
	[as date]
	[filepath string]
).

====================

[!] [Date And Time] methods follow.

====================

date
---

Result:
	[Date] new after milliseconds since origin [your milliseconds since origin] offset [your offset].

====================

time
---

[milliseconds a day]: [86400000].
[milliseconds since midnight]: [your milliseconds since origin]
	remainder of division by [milliseconds a day].

Result:
	[Time] new after milliseconds since midnight [milliseconds since midnight] offset [your offset].

====================

as date and time
---

Result:
	[You].

====================

as date
---

Result:
	Please, date.

====================

display on [stream]
---

[stream] display (Please, date).
[stream] space.
[stream] display (Please, time).

====================

filepath string
---

[date]: Please, date.
[time]: Please, time.
[offset sign]: ([your offset], is it positive?), if it is so {
	["p"].
}, or otherwise {
	["n"].
}.

Result:
	[String Stream] contents after {[stream] /
		[stream] display ([date] year) left padded up to [4] with ["0"].
		[stream] append ["_"].
		[stream] display ([date] month index) left padded up to [2] with ["0"].
		[stream] append ["_"].
		[stream] display ([date] day of month) left padded up to [2] with ["0"].
		[stream] append ["T"].
		[stream] display ([time] hour) left padded up to [2] with ["0"].
		[stream] append ["_"].
		[stream] display ([time] minute) left padded up to [2] with ["0"].
		[stream] append ["_"].
		[stream] display ([time] second) left padded up to [2] with ["0"].
		[stream] append ["Z"].
		[stream] append [offset sign].
		[stream] display [your offset] left padded up to [2] with ["0"].
	}.

====================

+ [y] years, [m] months, [d] days, [hh] hours, [mm] minutes and [ss] seconds
---

[milliseconds since origin]: Please,
	basic addition to years [y]
	months [m]
	days [d]
	hours [hh]
	minutes [mm]
	seconds [ss].

Result:
	[Date And Time] new after
		milliseconds since origin [milliseconds since origin]
		offset [your offset].

====================

basic addition to years [y] months [m] days [d] hours [hh] minutes [mm] seconds [ss]
---

#<[primitive call]; {
	[Calendar System] basic addition
		of milliseconds since origin [your milliseconds since origin]
		to years [y]
		months [m]
		days [d]
		hours [hh]
		minutes [mm]
		seconds [ss].
}>.

====================

[!] [Date And Time] factory methods follow.

====================

now
---

[milliseconds since origin]: Please, UTC milliseconds since origin at this instant.

Result:
	Please, new after
		milliseconds since origin [milliseconds since origin]
		offset [0].

====================

the [day of month] of month index [month index], year [year]
	at hour [hh] minute [mm] second [ss]
	offset [offset]
---

[utc milliseconds]: Please, 
	basic milliseconds since origin on
		day of month [day of month] month index [month index] year [year]
		at hour [hh] minute [mm] second [ss].
[offset milliseconds]: [offset] * [3600000].
[milliseconds]: [utc milliseconds] - [offset milliseconds].

Result:
	Please, new after
		milliseconds since origin [milliseconds]
		offset [offset].

====================

from string [string]
---

[milliseconds]: Please, basic from string [string].

Result:
	Please, new after milliseconds since origin [milliseconds] offset [0].

====================

UTC milliseconds since origin at this instant
---

#<[primitive call]; {
	[Calendar System] UTC milliseconds since origin at this instant.
}>.

====================

basic milliseconds since origin on
	day of month [day of month] month index [month index] year [year]
	at hour [hh] minute [mm] second [ss]
---

#<[primitive call]; {
	[Calendar System]
		milliseconds on
		year [year] month [month index] day [day of month]
		at hour [hh] minute [mm] second [ss].
}>.

====================

basic from string [string]
---

#<[primitive call]; {
	[Calendar System] milliseconds from string [string].
}>.

====================