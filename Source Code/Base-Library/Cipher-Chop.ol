[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Cipher-Chop] definition follows.

====================

It depends on ##([Object]).

It owns slots ##(
	[message id]
	[chop index]
	[contents]
	[stuff indices]
	[stuff]
).
It creates instance on ##([message id][chop index][contents][stuff indices][stuff]).
It reads slots ##([message id][chop index][contents][stuff indices][stuff]).

====================

[!] [Cipher-Chop] methods follow.

====================

= [chop]
---

Result:
	([your message id] = ([chop] message id)) and {
		([your chop index] = ([chop] chop index)) and {
		[your contents] = ([chop] contents).
	}.}.


====================

[!] [Cipher-Chop] factory methods follow.

====================

from bytes [byte array]
	integer length [integer length]
	contents length [contents length]
	stuff length [stuff length]
---

#<[validation]; {
	[byte array] is a [Sequence].
	[integer length] is a strict positive [Number].
	[contents length] is a strict positive [Number].
	[stuff length] is a strict positive [Number].
}>.

[stream]: [byte array] read stream. 
[read integer]: {
	[bytes]: [stream] next [integer length].
	[Number] from little endian bytes [bytes].
}.
[message id]: [read integer] proceed.
[chop index]: [read integer] proceed.
[stuff indices]: [stream] next [stuff length].
[stuffed length]: [stuff length] + [contents length].
[contents]: [Sequence] new instance.
[stuff]: [Sequence] new instance.
[Read header]{}: {
	[message id].
	[chop index].
	[stuff indices].
}.
[Read contents]{}: {
	[1] to [stuffed length] do {[i] /
		([stuff indices], does it include [i]?), if it is so {
			[stuff] add ([stream] next).
		}, or otherwise {
			[contents] add ([stream] next).
		}.
	}.
}.

[Read header].
[Read contents].

Result:
	[Cipher-Chop] new after
		message id [message id]
		chop index [chop index]
		contents [contents]
		stuff indices [stuff indices]
		stuff [stuff].

====================
