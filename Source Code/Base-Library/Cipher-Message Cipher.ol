[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Cipher-Message Cipher] definition follows.

====================

It depends on ##([Object]).

It owns slots ##(
	[random generator]
	[integer length]
	[contents length]
	[stuff length]
).
It creates instance on ##([configuration table][integer length][contents length][stuff length]).
It reads slots ##([integer length][contents length][stuff length]).

====================

[!] [Cipher-Message Cipher] methods follow.

====================

initialize after creation
---
[your random generator]= [Random Byte Generator] new instance.
[your integer length]= [4].
[your contents length]= [8].
[your stuff length]= [3].


====================

configuration table [configuration table]
---
[your random generator] configuration table [configuration table].

====================

configuration table
---
Result:
	[your random generator] configuration table.

====================

cipher document [byte array] after public key [seed integer]
---
[message chopper]: Please, new message chopper.
[chop composite]: [message chopper] chop bytes [byte array].
[bytes]: [chop composite]
		to bytes with [your integer length]
		and random generator (Please, stuffing random generator).

Result:
	Please, basic cipher bytes [bytes] after public key [seed integer].


====================

cipher document [byte array]
	after public key [seed integer]
	and configuration table [configuration table]
---

Please, configuration table [configuration table].

Result:
	Please, cipher document [byte array] after public key [seed integer].

====================

decipher document [byte array]
	after public key [seed integer]
	and configuration table [configuration table]
---

Please, configuration table [configuration table].

Result:
	Please, decipher document [byte array] after public key [seed integer].

====================

decipher document [byte array] after public key [seed integer]
---
[deciphered bytes]: Please, basic decipher bytes [byte array] after public key [seed integer].
[message chopper]: Please, new message chopper.
[chop composite]: [message chopper] reconstruct from bytes [deciphered bytes].
[document]: [chop composite] reconstruct document.

Result:
	[document].
====================

new message chopper
---
Result:
	[Cipher-Message Chopper] new after
		random generator (Please, stuffing random generator)
		integer length [your integer length]
		contents length [your contents length]
		stuff length [your stuff length].


====================

basic cipher bytes [bytes] after public key [seed integer]
---
[ciphered bytes]: [Sequence] new instance.
[cipher byte]: {[byte] /
		[r]: [your random generator] next.
		[ciphered]: [byte] + [r].
		[ciphered] remainder of division by [256].
}.
[Cipher all bytes]{}: {
		[your random generator] seed [seed integer].
		[bytes] each one do {[byte] /
			[ciphered byte]: [cipher byte] proceed with [byte].
			[ciphered bytes] add [ciphered byte].
		}.
	}.

[Cipher all bytes].

Result:
	[ciphered bytes].

====================

basic decipher bytes [bytes] after public key [seed]
---
[deciphered bytes]: [Sequence] new instance.
[decipher byte]: {[byte] /
		[r]: [your random generator] next.
		[deciphered]: [byte] - [r].
		[deciphered] remainder of division by [256].
}.
[decipher all bytes]{}: {
		[your random generator] seed [seed].
		[bytes] each one do {[byte] /
			[deciphered byte]: [decipher byte] proceed with [byte].
			[deciphered bytes] add [deciphered byte].
		}.
	}.

[decipher all bytes].

Result:
	[deciphered bytes].

====================
stuffing random generator
---
[generator]: [Random Byte Generator] new instance.
[generator] configuration table ##(
	[
		#(
			["source bytes"] -> ##([10] [20]);
			["source distortion"] -> ##([0]);
			["initial iteration count"] -> [100];
			["stepping"] -> ##([1]);
			["tail"] -> ##([1][1])
		)
	]
).
[generator] seed [1].

Result:
[generator].

====================

[!] [Cipher-Message Cipher] factory methods follow.

====================

cipher document [byte array]
	after public key [seed integer]
	and configuration table [configuration table]
---
[cipher]: Please, new instance.

Result:
	[cipher]
		cipher document [byte array]
		after public key [seed integer]
		and configuration table [configuration table].

====================

decipher document [byte array]
	after public key [seed integer]
	and configuration table [configuration table]
---
[cipher]: Please, new instance.

Result:
	[cipher]
		decipher document [byte array]
		after public key [seed integer]
		and configuration table [configuration table].



====================
