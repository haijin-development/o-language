[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Object] definition follows.

====================

[!] [Object] methods follow.

====================

initialize after creation
---

====================

configure new instance [procedure]
---
[instance]: Please, new instance.

[procedure] proceed with [instance].

Result:
	[instance].

====================

= [object]
---

Result:
	[you], is it identical to [object]?.

====================

=/= [object]
---

Result:
	([you] = [object]) not.

====================

, is it identical to [object]?
---

#<[primitive call]; {
	[Object System] is [you] identical to [object].
}>.

====================

, is it not identical to [object]?
---

Result:
	([you], is it identical to [object]?) not.

====================

, is it a [classification]?
---

#<[primitive call]; {
	[Object System] is [you] a [classification].
}>.

====================

, if it is something [procedure]
---

Result:
	[procedure] proceed with [you].

====================

, if it is nothing [procedure]
---

Result:
	[nothing].

====================

, is it nothing?
---

Result:
	[untruth].

====================


, is it something?
---

Result:
	[truth].

====================

, is it classification?
---

Result:
	[untruth].

====================

display string
---

Result:
	[String Stream] contents after {[stream] /
		Please, display on [stream].
	}.

====================

display on [stream]
---

It is another classification to implement the method.

====================

validate [procedure], or otherwise signal [error description]
---

Result:
	([procedure] proceed), if it is not so {
		Please, signal ([error description] proceed).
	}.

====================

signal [description]
---

[Error] signal [description].

====================

inspect
---

[Output] inspect [you].

====================

terminate program
---

#<[primitive call]; {
	[Object System] terminate program.
}>.

====================

metric symbol of reference
---

Result:
	[you].

====================

convert amount [amount] symbol [symbol] to your units
---

[thyself]: [you].

#<[validation]; {
	[symbol] = [thyself].
}>.

Result:
	[amount].

====================

[!] [Object] factory methods follow.

====================

new instance
---

[instance]: Please, basic new instance.
[instance] initialize after creation.

Result:
	[instance].

====================

basic new instance
---

#<[primitive call]; {
	[Object System] basic new instance of [you].
}>.

====================
