[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Ordered Set] definition follows.

====================

It depends on ##([Object]).
It owns slots ##(
	[elements]
).

====================

[!] [Ordered Set] methods follow.

====================

initialize after creation
---

[your elements]= ##().

====================

size
---

Result:
	[your elements] size.

====================

at [index]
---

Result:
	[your elements] at [index].

====================

add all [elements]
---

[elements] each one do {[element] /
	Please, add [element].
}.

Result:
	[elements].

====================

add [element]
---

[is it included?]: [You], does it include [element]?.

[is it included?], if it is not so {
	[your elements] add [element].
}.

Result:
	[element].

====================

= [indexed collection]
---

[size]: Please, size.
[range]: [1] to [size].
[same size]: [size] = ([indexed collection] size).
[same elements]{}: {
	[range], do all satisfy {[i] /
		([you] at [i]) = ([indexed collection] at [i]).
	}?.
}.

Result:
	[same size] and {[same elements].}.

====================

, does it include [element]?
---

Result:
	[your elements], does it include [element]?.

====================

display on [stream]
---

[stream] display [your elements].

====================

[!] [Ordered Set] factory methods follow.

====================

with all [collection]
---

Result:
	Please, configure new instance {[sequence] /
		[sequence] add all [collection].
	}.

====================