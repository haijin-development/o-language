[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Validation] definition follows.

====================

It depends on ##([Object]).

====================

[!] [Validation] factory methods follow.

====================

validate [object] named [name] is a [type]
---

[condition]: [object], is it a [type]?.

Please, validate condition [condition] or signal {
	"Expected [#([name])] to be a [#([type])]".	
}.

====================

validate [object] named [name] satisfies [procedure], description [description]
---

[condition]: [procedure] proceed.

Please, validate condition [condition] or signal {
	"Expected [#([name]) to #([description])]".	
}.

====================

validate [magnitude] named [name] is a strict positive [type]
---

[condition]: [magnitude] > [0].

Please, validate [magnitude] named [name] is a [type].
Please, validate condition [condition] or signal {
	"Expected [#([name])] to be strict positive".	
}.

====================

validate [value 1] named [name] = [value 2]
---

[condition]: [value 1] = [value 2].

Please, validate condition [condition] or signal {
	"Expected [#([name])] to equal #([value 2])".	
}.

====================

validate [collection] named [name] size is [number]
---

[condition]: ([collection] size) = [number].

Please, validate condition [condition] or signal {
	"Expected [#([name])] to equal #([number])".	
}.

====================

validate [collection] named [name] is a [type1] of [type2]
---

Please, validate [collection] named [name] is a [type1].
[collection] each one do {[each] /
	Please, validate [each] named ["element in #([name])"] is a [type2].
}.

====================

validate [listing] named [name] keys satisfy [procedure]
---

[condition]: ([listing] keys), do all satisfy [procedure]?.

Please, validate condition [condition] or signal {
	"Expected [#([name])] keys to satisfy condition.".	
}.

====================

validate [listing] named [name] values satisfy [procedure]
---

[condition]: ([listing] values), do all satisfy [procedure]?.

Please, validate condition [condition] or signal {
	"Expected [#([name])] keys to satisfy condition.".	
}.

====================

validate [filepath] named [name] file exists
---

[condition]: [filepath], does it exist?.

Please, validate condition [condition] or signal {
	"Expected [#([filepath] to string)] to exist.".	
}.

====================

validate condition [boolean] or signal [procedure]
---

[boolean], if it is not so {
	[description]: [procedure] proceed.

	Please, signal failure [description].
}.

====================

signal failure [message]
---

[Validation Failure] signal [message].

====================
