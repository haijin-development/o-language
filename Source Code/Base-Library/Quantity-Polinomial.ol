[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Polynomial] definition follows.

====================

It depends on ##([Object]).
It owns slots ##(
	[monomials]
).
It creates instance on ##(
	[monomials]
).
It reads slots ##(
	[monomials]
).
It caches calculation methods ##(
    [terms]
    [negated]
    [reciprocal]
).

====================

[!] [Polynomial] methods follow.

====================

initialize after monomials [monomials]
---

[your monomials]= [Sequence] with all [monomials].

====================

terms
---

Result:
    (Please, monomials) copy.

====================

display on [stream]
---

[your monomials] each one do {[monomial] /
	[stream] display [monomial].
} separated by {
	[stream] append [" + "].
}.

====================

+ [operand]
---

Result:
	[operand] added at right of [you].

====================

- [operand]
---

[negated]: [operand] negated.

Result:
    [you] + [negated].

====================

* [multiplier]
---

Result:
	[multiplier] multiplied at right of [you].

====================

/ [divisor]
---

[reciprocal]: [divisor] reciprocal.

Result:
    [you] * [reciprocal].

====================

negated
---

[terms]: [your monomials] proyect each one after {[each monomial] /
	[each monomial] negated.
}.

Result:
	[Polynomial] new after monomials [terms].

====================

reciprocal
---

Please, signal ["Polynomial is not closed on division operator."].

====================

(- ### Double dispatch methods ### -)

added at right of [operand]
---

Result:
	[operand] add polynomial [you].

====================

multiplied at right of [multiplier]
---

Result:
	[multiplier] multiply by polynomial [you].

====================

add number [number]
---

#<[validation]; {
    [number] is a [Number].
}>.

[monomial]: [Monomial] coefficient [number] and no variable.

Result:
    ([number], is it zero?), if it is so {
        [you].
    }, or otherwise {
        Please, add monomial [monomial].
    }. 

====================

add monomial [monomial]
---

#<[validation]; {
    [monomial] is a [Monomial].
}>.

[addition variable]: [Variable Assignment] on [monomial].
[terms]: ##().

[your monomials] each one do {[each monomial] /
	[are monomials additive?]: [monomial], is it additive to [each monomial]?.

	[are monomials additive?], if it is so {
		[addition]: [monomial] add monomial [each monomial]. 
		[addition variable] set [addition].
	}, or otherwise {
		[terms] add [each monomial].
	}.
}.
[is addition zero?]: ([addition variable] value), is it zero?.

[is addition zero?], if it is not so {
	[terms] add ([addition variable] value).	
}.

Result:
	[Polynomial] new after monomials [terms].

====================

add polynomial [polynomial]
---

#<[validation]; {
    [polynomial] is a [Polynomial].
}>.

[other monomials]: [polynomial] monomials.
[result]: [other monomials] accumulate value [you] after {[p][m] /
	[p] add monomial [m].
}.

Result:
	[result].

====================

multiply by number [number]
---

#<[validation]; {
    [number] is a [Number].
}>.

[terms]: [your monomials] proyect each one after {[monomial 1] /
	[monomial 1] multiply by number [number].
}.

Result:
    [Polynomial] new after monomials [terms].

====================

multiply by monomial [monomial]
---

#<[validation]; {
    [monomial] is a [Monomial].
}>.

[result]: [your monomials] accumulate value [0] after {[partial][monomial 1] /
	[term]: [monomial 1] * [monomial].
	[partial] + [term].
}.

Result:
    [result].

====================

multiply by polynomial [polynomial]
---

#<[validation]; {
    [polynomial] is a [Polynomial].
}>.

[result]: ([polynomial] monomials) accumulate value [0] after {[partial][monomial] /
	[term]: [you] * [monomial].
	[partial] + [term].
}.

Result:
    [result].

====================

[!] [Polynomial] factory methods follow.

====================

with all [monomials]
---

Result:
	Please, new after monomials [monomials].

====================