[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Sequence] definition follows.

====================

It depends on ##([Object]).
====================

[!] [Sequence] methods follow.

====================

size
---

#<[primitive call]; {
	[Sequence System] size of [you].
}>.

====================

, is it empty?
---

Result:
	(Please, size) = [0].

====================

, is it not empty?
---

Result:
	([you], is it not empty?) not.

====================

= [sequence]
---

[size]: Please, size.
[range]: [1] to [size].
[same size]: [size] = ([sequence] size).
[same elements]{}: {
	[range], do all satisfy {[i] /
		([you] at [i]) = ([sequence] at [i]).
	}?.
}.

Result:
	[same size] and {[same elements].}.

====================

first
---

Result:
	Please, at [1].
====================

second
---

Result:
	Please, at [2].

====================

third
---

Result:
	Please, at [3].

====================

fourth
---

Result:
	Please, at [4].

====================


last
---

Result:
	Please, at (Please, size).

====================


at [index]
---

#<[primitive call]; {
	[Sequence System] sequence [you] at [index].
}>.

====================

at [index] set [value]
---

#<[primitive call]; {
	[Sequence System] sequence [you] at [index] set [value].
}>.

====================

index of [element]
---

Result:
	Please, index of [element] or, if it is absent {
		Please, signal ["Absent element."].
	}.

====================

index of [element] or, if it is absent [procedure]
---

#<[primitive call]; {
	[Sequence System] sequence [you] index of [element] if absent [procedure].
}>.

====================

add [element]
---

#<[primitive call]; {
	[Sequence System] sequence [you] add [element].
}>.

====================

add all [elements]
---

Please, add all [elements] after index (Please, size).

Result:
	[elements].

====================

add first [element]
---

Result:
	Please, add [element] after index [0].

====================

add [element] after index [index]
---

Please, add all ([Sequence] with [element]) after index [index].

Result:
	[element].

====================

add all [elements] after index [index]
---

#<[primitive call]; {
	[Sequence System] sequence [you] add all [elements] after index [index].
}>.

====================

copy
---

#<[primitive call]; {
	[Sequence System] copy [you].
}>.

====================

copy from [start index] up to [end index]
---

#<[primitive call]; {
	[Sequence System] sequence [you] copy from [start index] up to [end index].
}>.

====================

all but last one
---

Result:
	((Please, size) = [1]), if it is so {
		##().
	}, or otherwise {
		Please, copy from [1] up to ((Please, size) - [1]).
	}.

====================

all but first one
---

Result:
	((Please, size) = [1]), if it is so {
		##().
	}, or otherwise {
		Please, copy from [2] up to ((Please, size)).
	}.

====================

each one do [procedure]
---

#<[primitive call]; {
	[Sequence System] sequence [you] each one do [procedure].
}>.

====================

each one do [procedure] separated by [procedure 2]
---

[first one variable]: [Variable Assignment] on [truth].

Please, each one do {[each] /
	([first one variable] value), if it is so {
		[first one variable] set [untruth].
	}, or otherwise {
		[procedure 2] proceed.
	}.
	[procedure] proceed with [each].
}.

====================

select those such that [procedure]
---

#<[primitive call]; {
	[Sequence System] sequence [you] select those such that [procedure].
}>.

====================

select those such that [selection procedure], afterwards do [procedure]
---

[selection]: Please, select those such that [selection procedure].

Result:
	[selection] each one do [procedure].

====================

first one such that [procedure]
---

Result:
	Please, first one such that [procedure], or, if there is none {
		Please, signal ["Element is absent."].
	}.

====================

first one such that [procedure], or, if there is none [absent]
---

Result:
	Please, first one such that [procedure],
		if there is one { [found]/ [found]. },
		if there is none [absent].

====================

first one such that [procedure], if there is one [present], if there is none [absent]
---

#<[primitive call]; {
	[Sequence System] sequence [you] find [procedure] if present [present] if absent [absent].
}>.
====================

proyect each one after [procedure]
---

#<[primitive call]; {
	[Sequence System] sequence [you] proyect each one after [procedure].
}>.
====================


accumulate value [starting value] after [procedure]
---

#<[primitive call]; {
	[Sequence System] sequence [you] accumulate value [starting value] after [procedure].
}>.

====================

accumulate into [collection] after [procedure]
---

Please, each one do {[each] /
	[procedure] proceed with [collection] and with [each].
}.

Result:
	[collection].

====================

, does any one satisfy [procedure]?
---

#<[primitive call]; {
	[Sequence System] sequence [you] any one satisfy [procedure].
}>.

====================

, do all satisfy [procedure]?
---

#<[primitive call]; {
	[Sequence System] sequence [you] all satisfy [procedure].
}>.

====================

remove first
---

Result:
	Please, remove element at index [1]. 
====================

remove last
---

Result:
	Please, remove element at index (Please, size). 
====================

remove last [n]
---

[index]: ((Please, size) - [n]) + [1].

Result:
	Please, remove [n] elements at index [index]. 
====================

remove element at index [index]
---

[element]: Please, at [index].

[element].
Please, remove [1] elements at index [index].

Result:
	[element].

====================

remove [n] elements at index [index]
---

#<[primitive call]; {
	[Sequence System] sequence [you] remove [n] elements at index [index].
}>.

====================

concatenated to [sequence]
---

[copy]: Please, copy.

[copy] add all [sequence].

Result:
	[copy].
====================


left padded up to [n] with [element]
---

[padding count]: ([n] - (Please, size)) max [0].
[padding]: [Sequence] with [element] repeated [padding count] times.

Result:
	[padding] concatenated to [you].
====================

right padded up to [n] with [element]
---

[padding count]: ([n] - (Please, size)) max [0].
[padding]: [Sequence] with [element] repeated [padding count] times.

Result:
	[you] concatenated to [padding].
====================

, does it include [element]?
---

Result:
	[you], does any one satisfy {[each] /
		[each], is it identical to [element]?.
	}?.
====================

shuffle after [random number generator]
---
[size]: Please, size.
[iteration count]: [size] * [3].

[iteration count] times repeat {
	[i]: [random number generator] integer between [1] and [size].
	[j]: [random number generator] integer between [1] and [size].
	[e1]: Please, at [i].
	[e2]: Please, at [j].
	[pick two elements]{}: {
		[e1].
		[e2].
	}.
	[pick two elements].
	Please, at [i] set [e2].
	Please, at [j] set [e1].
}.
====================

empty write stream
---

Result:
	[Stream] on ([Sequence] new instance).

====================

read stream
---

Result:
	[Stream] on [you].

====================

sort after [procedure]
---

Please, sort after comparison {[o1][o2] /
	[v1]{}: {
		[procedure] proceed with [o1].
	}.
	[v2]{}: {
		[procedure] proceed with [o2].
	}.
	[v1] <= [v2].
}.

====================

sort after comparison [two elements comparison]
---

#<[primitive call]; {
	[Sequence System] sequence [you] sort after comparison [two elements comparison].
}>.

====================

sorted after [procedure]
---

[copy]: Please, copy.

[copy] sort after [procedure].

Result:
	[copy].

====================

display on [stream]
---

[stream] append ["##"].
[stream] append ["("].
Please, each one do {[each] /
	[stream] display [each].
}.
[stream] append [")"].

====================

inspect
---

[Output] print ["##"].
[Output] print ["("].
Please, each one do {[each] /
	[each] inspect.
}.
[Output] print [")"].
[Output] cr.

====================

[!] [Sequence] factory methods follow.

====================

basic new instance
---

#<[primitive call]; {
	[Sequence System] basic new instance of [you].
}>.

====================


with [element]
---

Result:
	Please, configure new instance {[sequence] /
		[sequence] add [element].
	}.

====================

with [element] repeated [n] times
---

Result:
	Please, configure new instance {[sequence] /
		[n] times repeat {
			[sequence] add [element].
		}.
	}.


====================

with all [collection]
---

Result:
	Please, configure new instance {[sequence] /
		[sequence] add all [collection].
	}.

====================