[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Listing] definition follows.

====================

It depends on ##([Object]).

====================

[!] [Listing] methods follow.

====================

size
---

#<[primitive call]; {
	[Listing System] size of [you].
}>.

====================

, is it empty?
---

Result:
	(Please, size) = [0].

====================

= [listing]
---

It remains to implement.

====================

at [key]
---

Result:
	Please, at [key],
		if it is present {[value] / [value].},
		if it is absent {
			Please, signal ["Key [#([key])] is absent."].	
		}.

====================

at [key], if it is present [present], if it is absent [absent]
---

#<[primitive call]; {
	[Listing System] listing [you] at [key] if present [present] if absent [absent].
}>.

====================

at [key], or, if it is absent [absent]
---

Result:
	Please, at [key], if it is present {[value] / [value].}, if it is absent [absent].

====================

at [key] set [value]
---

#<[primitive call]; {
	[Listing System] listing [you] at [key] set [value].
}>.

====================

keys
---

#<[primitive call]; {
	[Listing System] listing [you] keys.
}>.


====================

values
---

#<[primitive call]; {
	[Listing System] listing [you] values.
}>.

====================

remove key [key]
---

#<[primitive call]; {
	[Listing System] remove from [you] key [key].
}>.

====================

, does it define key [key]?
---

#<[primitive call]; {
	[Listing System] listing [you] defines key [key].
}>.

====================

if key [string] is absent do [procedure]
---

Result:
	([you], does it define key [string]?), if it is not so [procedure].

====================

each key and value do [procedure]
---

#<[primitive call]; {
	[Listing System] each key and value in [you] do [procedure].
}>.

====================

copy
---

[copy]: [Listing] new instance.

Please, each key and value do {[key][value] /
	[copy] at [key] set [value].
}.

Result:
	[copy].

====================

display on [stream]
---

[stream] append ["#"].
[stream] append ["("].
Please, each key and value do {[key][value] /
	[stream] display [key].
	[stream] append ["->"].
	[stream] display [value].
	[stream] append [";"].
}.
[stream] append [")"].

====================

[!] [Listing] factory methods follow.

====================

basic new instance
---

#<[primitive call]; {
	[Listing System] basic new instance of [you].
}>.

====================

with all keys in [listing]
---

Result:
	Please, configure new instance {[instance] /
		[listing] each key and value do {[key][value] /
			[instance] at [key] set [value].
		}.
	}.

====================