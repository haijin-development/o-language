[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Units Product] definition follows.

====================

(-
	Object for assisting during the product of variables under metric systems equivalence.
	The object complexity may be an indication for the lack one or more abstractions in the model as it is.
-)

It depends on ##([Object]).
It owns slots ##(
	[coefficient]
	[symbols]
	[exponents]
	[canonical symbols]
).

====================

[!] [Units Product] methods follow.

====================

initialize after creation
---

[your coefficient]= [1].
[your symbols]= ##().
[your exponents]= ##().
[your canonical symbols]= ##().

====================

*= coefficient [number]
---
(-
	Multiply [your coefficient] by [number].
-)

[your coefficient]= [your coefficient] * [number].

====================

set units in [variable bag]
---
(-
	Set all units present in [variable bag].
	The operation sets which units to convert to during addition.
-)

Please, initialize after creation.

[variable bag] each symbol and exponent do {[symbol][exponent] /
	[canonical symbol]: [symbol] metric symbol of reference.

	[your symbols] add [symbol].
	[your exponents] add [exponent].
	[your canonical symbols] add [canonical symbol].
}.

====================

set coefficient [coefficient]
---

[your coefficient]= [coefficient].

====================

+= monomial [monomial]
---
(-
	Add [monomial] to current value converting units as necessary.
-)

[coefficient variable]: [Variable Assignment] on ([monomial] coefficient).
[variable bag]: [monomial] variable bag.

[variable bag] each symbol and exponent do {[symbol][exponent] /
	[conversion coefficient]: [you] + symbol [symbol], exponent [exponent].
	[coefficient variable] *= [conversion coefficient].
}.

[your coefficient]= [your coefficient] + ([coefficient variable] value).

====================

+ symbol [symbol], exponent [exponent]
---
(-
	Answer [symbol], [exponent] units conversion coefficient.
-)

[canonical symbol]: [symbol] metric symbol of reference.
[index]: [your canonical symbols] index of [canonical symbol].
[present symbol]: [your symbols] at [index].
[present exponent]: [your exponents] at [index].

[conversion factor]: [present symbol] convert amount [1] symbol [symbol] to your units.
[conversion coefficient]: [conversion factor] raised to [exponent].

#<[validation]; {
	[symbol] satisfies {
		[you], does it include canonical symbol [symbol]?.
	}, description {["be included as unit"].}.
}>.

Result:
	[conversion coefficient].

====================

*= variable bag [variable bag]
---
(-
	Multiply all units in [variable bag] converting units as necessary.
-)

[variable bag] each symbol and exponent do {[symbol][exponent] /
	[you] *= symbol [symbol], exponent [exponent].
}.

====================

*= symbol [symbol], exponent [exponent]
---
(-
	Multiply [symbol] [exponent] converting units as necessary.
-)

[canonical symbol]: [symbol] metric symbol of reference.
[does it include symbol?]: [you], does it include canonical symbol [canonical symbol]?.

[does it include symbol?], if it is so {
	Please, *= symbol [symbol] and exponent [exponent] as present units.
}, or otherwise {
	Please, *= first ocurrence of symbol [symbol] and exponent [exponent].
}.

====================

*= first ocurrence of symbol [symbol] and exponent [exponent]
---
(-
	Set the first occurrence of [symbol] to [exponent].
-)

[canonical symbol]: [symbol] metric symbol of reference.

[your symbols] add [symbol].
[your exponents] add [exponent].
[your canonical symbols] add [canonical symbol].

====================

*= symbol [symbol] and exponent [exponent] as present units
---
(-
	Add [symbol] current exponent to [exponent] converting units as necessary.
-)

[canonical symbol]: [symbol] metric symbol of reference.
[index]: [your canonical symbols] index of [canonical symbol].
[present symbol]: [your symbols] at [index].
[present exponent]: [your exponents] at [index].

[conversion factor]: [present symbol] convert amount [1] symbol [symbol] to your units.
[conversion coefficient]: [conversion factor] raised to [exponent].

[your coefficient]= [your coefficient] * [conversion coefficient].
[your exponents] at [index] set ([present exponent] + [exponent]).

====================

, does it include canonical symbol [symbol]?
---

[canonical symbol]: [symbol] metric symbol of reference.

Result:
	[your canonical symbols], does it include [canonical symbol]?.

====================

as monomial
---

[variable powers]: ##().
[variable bag]: [Variable Bag] new after variable powers [variable powers].

[1] to ([your symbols] size) do {[i] /
	[variable power]: [Variable Power]
		symbol ([your symbols] at [i])
		exponent ([your exponents] at [i]).
	[variable powers] add [variable power].
}.

Result:
	[Monomial] new after coefficient [your coefficient] variable bag [variable bag].

====================

[!] [Units Product] factory methods follow.

====================

create monomial after [procedure]
---

[units product]: Please, new instance.

[procedure] proceed with [units product].

Result:
	[units product] as monomial.

====================