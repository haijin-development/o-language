[timestamp string]:
	([Date And Time] now) filepath string.

[parameter folder]:
	[Directory Path] from string ["./Simulation Parameter"].

[output folder]:
	([Directory Path] from string ["../Simulation Output"])
		append string [timestamp string].

[simulation context]:
	[Nutrition Plan Simulation Context] new after
		parameter folder [parameter folder]
		output folder [output folder].

[Run simulation]{}: {
	[output folder] ensure directory existance.
	[Simulation] proceed with simulation context [simulation context].
}.

{
	[Run simulation].
} on error do {[error] /
	[Output] display ([error] description).	
}.