[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Nutrition Plan Simulation Context] definition follows.

====================

It depends on ##([Simulation Context]).
It owns static slots ##(
	[action listing]
).
It owns slots ##(
	[event rule registry]
	[income rule registry]
	[diet rule registry]
	[dish ingredient registry]
	[grocery rule registry]
	[stock rule registry]
	[currency exchange registry]
	[diet substitution rule registry]
	[accounting book]
).
It reads slots ##(
	[event rule registry]
	[income rule registry]
	[diet rule registry]
	[dish ingredient registry]
	[grocery rule registry]
	[stock rule registry]
	[currency exchange registry]
	[diet substitution rule registry]
	[accounting book]
).
It initializes after {
	[your action listing]= #(
		["Mostrar texto de simulacion"] -> [Display Step Text];
		["Registrar eventos periodicos"] -> [Register Frequent Events];
		["Recibir ingresos y productos"] -> [Receive Groceries And Products];
		["Adquirir productos"] -> [Purchase Groceries];
		["Consumir productos"] -> [Consume Meals And Products]
	).
}.

====================

[!] [Nutrition Plan Simulation Context] methods follow.

====================

read all simulation parameters
---

Previous read all simulation parameters.

[your accounting book]= [Accounting Book] new instance.
[your currency exchange registry]= [Currency Exchange Registry] from file in [your parameter folder].
[your dish ingredient registry]= [Dish Ingredient Registry] from file in [your parameter folder].
[your diet rule registry]= [Diet Rule Registry] from file in [your parameter folder].
[your diet substitution rule registry]= [Diet Substitution Rule Registry] from file in [your parameter folder].
[your income rule registry]= [Income Rule Registry] from file in [your parameter folder].
[your grocery rule registry]= [Grocery Rule Registry] from file in [your parameter folder].
[your stock rule registry]= [Stock  Rule Registry] from file in [your parameter folder].
[your event rule registry]= [Event Rule Registry] from file in [your parameter folder].

[Currency Conversion Table] rate table ([your currency exchange registry] rate table).

====================

step action named [action name]
---

Result:
	[your action listing] at [action name].

====================

, are all [dish] ingredients present?
---

[accounting book]:
	Please, accounting book.
[ingredients]:
	[dish] ingredients.
[absent]:
	[accounting book] select those ingredientes in [ingredients] which lack of stock.

Result:
	([absent] size) = [0].

====================

select those ingredientes in [dish] which lack of stock
---

[accounting book]:
	Please, accounting book.
[ingredients]:
	[dish] ingredients.

Result:
	[accounting book] select those ingredientes in [ingredients] which lack of stock.

====================

consume ingredient [ingredient]
---

[accounting book]:
	Please, accounting book.

[accounting book] substract ([ingredient] as monomial) from stock.

====================

register the lack of ingredients for comsumption of [dish]
---

[date]:
	Please, simulation date.
[lacking of]:
	Please, select those ingredientes in [dish] which lack of stock.
[event]: [Write Lack Of Ingredient]
	new after date [date]
		dish [dish]
		lacking of [lacking of].

[Journal] add [event].

====================

available money
---

[accounting book]:
	Please, accounting book.

Result:
	[accounting book] available money.

====================

price of [ingredient]
---

[grocery rule registry]:
	Please, grocery rule registry.

Result:
	[grocery rule registry] price of [ingredient].

====================

all purchase rules
---

[grocery rule registry]:
	Please, grocery rule registry.

Result:
	[grocery rule registry] all rules.

====================

add all [income] to stock
---

[accounting book]:
	Please, accounting book.

[accounting book] add all [income] to stock.

====================


add [ingredient] to stock
---

[accounting book]:
	Please, accounting book.
[date]:
	Please, simulation date.
[price]:
	Please, price of [ingredient].

[accounting book] on [date] add [ingredient] at price [price] to stock.

====================

stock of [ingredient]
---

[accounting book]:
	Please, accounting book.

Result:
	[accounting book] stock of [ingredient].

====================

min stock of [ingredient]
---

[stock rule registry]:
	Please, stock rule registry.

Result:
	[stock rule registry] min stock of [ingredient].

====================

max stock of [ingredient]
---

[stock rule registry]:
	Please, stock rule registry.

Result:
	[stock rule registry] max stock of [ingredient].

====================

all substitutions to [dish]
---

#<[validation]; {
	[dish] is a [Dish].
}>.

[accounting book]:
	Please, accounting book.
[diet substitution rule registry]:
	Please, diet substitution rule registry.

Result:
	[diet substitution rule registry] all substitutions to [dish].

====================