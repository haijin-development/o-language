[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Currency Exchange Registry] definition follows.

====================

It depends on ##([Registry]).
It owns slots ##(
	[rate table]
).
It reads slots ##(
	[rate table]
).
It creates instance on ##(
	[rate table]
).
It initializes after {
	[your filename]= ["Currency Exchange.json"].
}.

====================

[!] [Currency Exchange Registry] factory methods follow.

====================

from struct [struct]
---

[rate table]:
	#().
[Add rate of]: {[currency struct] /
		[reference currency]:
			[Currency Metric System] metric symbol of reference.
		[currency name]:
			[currency struct] at ["Moneda"].
		[coefficient]:
			[currency struct] at ["Cotizacion en ARS"].

		[currency]:
			[Object Registry] units symbol at [currency name].
		[conversion amount]:
			[coefficient] of [reference currency].

		[rate table] at [currency] set [conversion amount].
}. 

[struct] each one do {[currency struct] /
		[Add rate of] proceed with [currency struct].
	}.

Result:
	Please, new after rate table [rate table].

====================