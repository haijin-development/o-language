[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Simulation] definition follows.

====================

It depends on ##([Object]).
It owns slots ##(
	[simulation context]
).
It is callable procedure on ##(
	[simulation context]
).

====================

[!] [Simulation] methods follow.

====================

proceed
---

Please, read all simulation parameters.
Please, execute simulation loop.
Please, write all reports down.

====================

execute simulation loop
---

[start date]:
	[your simulation context] start date.
[end date]:
	[your simulation context] end date.
[actions]:
	[your simulation context] all step actions.
[Update simulation date]: {[date] /
	[your simulation context] simulation date [date].
}.
[Step]{}: {
	Please, step with [actions].	
}.

[start date] to [end date], each date do {[date] /
	[Update simulation date] proceed with [date].
	[Step].
}.

====================

step with [actions]
---

[actions] each one do {[action] /
	[action] proceed.
}.

====================

read all simulation parameters
---

[your simulation context] read all simulation parameters.

====================

write all reports down
---

[your simulation context] write all reports down.

====================