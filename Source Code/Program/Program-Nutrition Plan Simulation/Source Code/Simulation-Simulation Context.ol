[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Simulation Context] definition follows.

====================

It depends on ##([Object]).
It owns slots ##(
	[parameter folder]
	[output folder]
	[simulation configuration]
	[simulation date]
).
It creates instance on ##(
	[parameter folder]
	[output folder]
).
It reads slots ##(
	[simulation date]
).
It writes slots ##(
	[simulation date]
).

====================

[!] [Simulation Context] methods follow.

====================

read all simulation parameters
---

[configuration filepath]:
	[your parameter folder] append filename ["Simulation Configuration.json"].

[your simulation configuration]=
	[Simulation Configuration] at [configuration filepath].

====================

start date
---

Result:
	[your simulation configuration] start date.

====================

end date
---

Result:
	[your simulation configuration] end date.

====================

all step actions
---

[action names]:
	[your simulation configuration] all step actions.
[actions]:
	[action names] proyect each one after {[action name] /
		Please, new action named [action name].
	}.

Result:
	[actions].

====================

new action named [action name]
---

[action type]: Please, step action named [action name].

Result:
	[action type] new after simulation [you].

====================

step action named [action name]
---

It is another classification to implement the method.

====================

write all reports down
---

[Journal] write all writable events into [your output folder].

====================