[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Purchase Groceries] definition follows.

====================

It depends on ##([Simulation Step]).
It owns slots ##(
	[purchase set]
).

====================

[!] [Purchase Groceries] methods follow.

====================

initialize after creation
---

[your purchase set]= [Set] new instance.

====================

proceed
---

Please, update ingredient eligibility according to stock.
Please, acquire eligible ingredient.

====================

update ingredient eligibility according to stock
---

[all rules]:
	[your simulation] all purchase rules.

[all rules] each one do {[rule] /
	Please, update ingredient ([rule] ingredient) elegibility according to stock.
}.

====================

acquire eligible ingredient
---

[available money]:
	[your simulation] available money.
[purchase]:
	Please, most prioritised ingredients acquirable with [available money].

[purchase] each one do {[ingredient] /
	[your simulation] add [ingredient] to stock.
}.

====================

update ingredient [ingredient] elegibility according to stock
---

[stock]:
	[your simulation] stock of [ingredient].
[min stock]:
	[your simulation] min stock of [ingredient].
[max stock]:
	[your simulation] max stock of [ingredient].
[is it below min stock?]:
	[stock] <= [min stock].
[is it above max stock?]:
	[stock] > [max stock].

[is it below min stock?], if it is so {
	Please, make [ingredient] eligible.
}.
[is it above max stock?], if it is so {
	Please, make [ingredient] not eligible.
}.

====================

most prioritised ingredients acquirable with [amount of money]
---

[all rules]:
	[your simulation] all purchase rules.
[available money]:
	[Variable Assignment] on [amount of money].
[purchase]:
	##().
[Purchase after rule]: {[rule] /
	[ingredient]:
		[rule] ingredient.
	[expense]:
		[rule] expense.

	[purchase] add [ingredient].
	[available money] -= [expense].
}.

[all rules] each one do {[rule] /
	[may it purchase it?]:
		Please, may it purchase after [rule] with ([available money] value)?.

	[may it purchase it?], if it is so {
		[Purchase after rule] proceed with [rule].
	}.
}.

Result:
	[purchase].

====================

may it purchase after [rule] with [available money]?
---

[expense]:
	[rule] expense.
[ingredient]:
	[rule] ingredient.
[is it affordable?]:
	[expense] <= [available money].
[is it eligible?]:
	Please, is [ingredient] eligible?.

Result:
	[is it eligible?] and {
		[is it affordable?].
	}.

====================

is [ingredient] eligible?
---

Result:
	[your purchase set], does it include [ingredient]?.

====================

make [ingredient] eligible
---

[your purchase set] add [ingredient].

====================

make [ingredient] not eligible
---

[your purchase set] remove [ingredient].

====================