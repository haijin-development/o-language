[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Accounting Book] definition follows.

====================

It depends on ##([Object]).
It owns slots ##(
	[movements]
	[snapshot]
).

====================

[!] [Accounting Book] methods follow.

====================

initialize after creation
---

[your movements]=
	[Sequence] new instance.
[your snapshot]=
	[0].

====================

add all [ingredients] to stock
---

[ingredients] each one do {[ingredient]/
	Please, add ([ingredient] as monomial) to stock.
}.

====================

add [quantity] to stock
---

#<[validation]; {
	[quantity] is a [Monomial].
}>.

[addition movement]:
	[Accounting Book Addition] new after ingredient [quantity].

[your movements] add [addition movement].
[your snapshot]= [your snapshot] + [quantity].

====================

substract [quantity] from stock
---

#<[validation]; {
	[quantity] is a [Monomial].
}>.

[quantity negated]:
	[quantity] negated.

Please, add [quantity negated] to stock.

====================

on [date] add [ingredient] at price [price] to stock
---

#<[validation]; {
	[date] is a [Date].
	[ingredient] is a [Dish Ingredient].
	[price] is a [Monomial].
}>.

[expense]:
	([ingredient] as monomial) * [price].
[Register purchase event]{}: {
	[event]:
		[Write Purchase] new after date [date] ingredient [ingredient] expense [expense].

	[Journal] add [event].
}.

Please, add ([ingredient] as monomial) to stock.
Please, substract [expense] from stock.
[Register purchase event].

====================

stock snapshot
---

Result:
	[your snapshot].

====================

available money
---

[currency amount]:
	[1] of [USD].

[polynomial terms]:
	[your snapshot] terms.

Result:
	[polynomial terms] first one such that {[term] /
		[term], is it additive to [currency amount]?.
	}, or, if there is none {
		[0].
	}.

====================

stock of [ingredient]
---

#<[validation]; {
	[ingredient] is a [Dish Ingredient].
}>.

[polynomial terms]:
	[your snapshot] terms.
[monomial]:
	[ingredient] as monomial.

Result:
	[polynomial terms] first one such that {[term] /
		[term], is it additive to [monomial]?.
	}, or, if there is none {
		[0].
	}.

====================

, is ingredient [ingredient] present?
---

#<[validation]; {
	[ingredient] is a [Dish Ingredient].
}>.

[stock]:
	Please, stock of [ingredient].

Result:
	([stock] - ([ingredient] as monomial)), is it strict positive?.

====================

, is ingredient [ingredient] absent?
---

#<[validation]; {
	[ingredient] is a [Dish Ingredient].
}>.

Result:
	([you], is ingredient [ingredient] present?) not.

====================

select those ingredientes in [ingredient set] which lack of stock
---

#<[validation]; {
	[ingredient set] is a [Sequence] of [Dish Ingredient].
}>.

Result:
	[ingredient set] select those such that {[ingredient] /
		[you], is ingredient [ingredient] absent?.
	}.

====================