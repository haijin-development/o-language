[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Consume Meals And Products] definition follows.

====================

It depends on ##([Simulation Step]).

====================

[!] [Consume Meals And Products] methods follow.

====================

proceed
---

Please, each diet rule active on date do {[diet rule] /
	Please, consume all dishes in [diet rule].
}.

====================

consume all dishes in [diet rule]
---

[dishes]:
	[diet rule] all dishes.

Please, consume all dishes [dishes].

====================

each diet rule active on date do [procedure]
---

[date]:
	Please, simulation date.
[diet rule registry]:
	[your simulation] diet rule registry.
[diet rules]:
	[diet rule registry] all active on date [date].

[diet rules] each one do [procedure].

====================

consume all dishes [dishes]
---

[dishes] each one do {[dish] /
	Please, consume dish [dish] or a substitute of it.
}.

====================

consume dish [dish] or a substitute of it
---

[are all dish ingredients present?]:
	[your simulation], are all [dish] ingredients present?.

[are all dish ingredients present?], if it is so {
	Please, consume dish [dish].
}, or otherwise {
	Please, substitute [dish] by another dish.
}.

====================

substitute [dish] by another dish
---

[substitutions]:
	[your simulation] all substitutions to [dish].

[substitutions] first one such that {[substitute dish] /
	Please, may it substitute main dish by [substitute dish]?.
}, if there is one {[substitute dish] /
	Please, consume dish [substitute dish].
	Please, register [dish] substitution by [substitute dish].
}, if there is none {
	Please, register the lack of ingredients for comsumption of [dish].
}.

====================

may it substitute main dish by [substitution dish]?
---

Result:
	[your simulation], are all [substitution dish] ingredients present?.

====================

register the lack of ingredients for comsumption of [dish]
---

[your simulation] register the lack of ingredients for comsumption of [dish].

====================

register [dish] substitution by [substitute dish] 
---

[date]:
	Please, simulation date.
[event]:
	[Write Substitution] new after date [date] dish [dish] substitute dish [substitute dish].

[Journal] add [event].

====================

consume dish [dish]
---

[Consume Dish] proceed with simulation [your simulation] dish [dish].

====================