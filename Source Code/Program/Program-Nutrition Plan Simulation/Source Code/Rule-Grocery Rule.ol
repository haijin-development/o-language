[!] [License]

Se autoriza la plataforma Bitbucket para la lectura, para la copia y para la indexación de este software, total o parcialmente.

Se autoriza la lectura, el uso o la realización de trabajos derivados de este software a cualquier individuo o institución que cumpla los siguientes requisitos:

- Aquel individuo o institución que no consumió, no ofertó, no publicitó ni obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución no consumió trabajo sexual, no ofertó trabajo sexual, no publicitó trabajo sexual ni obtuvo beneficios derivados de trabajo sexual durante los pasados 5 años.

- Aquel el individuo o institución que consumió, ofertó, publicitó u obtuvo renta de trabajo sexual deberá incluir en cada trabajo derivado de este sofware el texto claro, legible y visible

    Esta oficina, individuo o institución consumió trabajo sexual, ofertó trabajo sexual, publicitó trabajo sexual u obtuvo beneficios derivados de trabajo sexual en los pasados 5 años.

El texto claro, legible y visible deberá estar presente hasta 5 años transcurridos desde el uso más reciente del trabajo derivado de este software en todo producto que incluya el trabajo derivado de este software: páginas web, oficinas comerciales, diarios o revistas, programas de computadoras, apps de celulares, manuales e instrucciones de uso, etc.

En todo otro caso se prohibe el uso gratuito y libre y la creación de trabajos derivados de este software sin la solicitud escrita explícita y la aprobación escrita explícita de su autor.

En todos los casos, el autor de este software deslinda toda responsabilidad derivada de su uso.

====================

[!] [Author]

[Author]: Martín Rubí.
[Contact]: haijin.development@gmail.com.
[Date]: 2025.

====================

[!] [Grocery Rule] definition follows.

====================

It depends on ##([Object]).
It owns slots ##(
    [ingredient]
    [price]
).
It creates instance on ##(
    [ingredient]
    [price]
).
It reads slots ##(
    [ingredient]
    [price]
).

====================

[!] [Grocery Rule] methods follow.

====================

expense
---

Result:
    ([your ingredient] as monomial) * [your price].

====================

[!] [Grocery Rule] factory methods follow.

====================

from struct [struct]
---

[ingredient]: [Dish Ingredient] from struct [struct].

[price coefficient]: ([struct] at ["Precio"]) at ["Monto"].
[price units]: ([struct] at ["Precio"]) at ["Moneda"].

[units]: [struct] at ["Unidades"], or, if it is absent {[""].}.
[currency]: [Object Registry] units symbol at [price units].
[price]: ([price coefficient] of [currency]) / ([ingredient] as monomial).

Result:
    Please, new after ingredient [ingredient] price [price].

====================